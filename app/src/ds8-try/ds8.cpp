#include <iostream>
#define REPORT( X )   std::cout << #X << " = " << ( X ) << "\n"

#include <sstream>
#include <string>

#include "D128API.h"

#include <Windows.h>

#define PROCTYPE_INIT    DGD128_Initialise
#define PROCNAME_INIT   "DGD128_Initialise"
#define PROCTYPE_UPDATE  DGD128_Update
#define PROCNAME_UPDATE "DGD128_Update"
#define PROCTYPE_CLOSE   DGD128_Close
#define PROCNAME_CLOSE  "DGD128_Close"

PROCTYPE_INIT   D128_Init   = NULL;
PROCTYPE_UPDATE D128_Update = NULL;
PROCTYPE_CLOSE  D128_Close  = NULL;

#define FAIL( RETURN_VALUE, STUFF )   { std::stringstream _ss; _ss << STUFF; mError = _ss.str(); return RETURN_VALUE; } 
#define CHECK( STUFF ) \
    if( procError   ) FAIL( procError,   PROCNAME_UPDATE << "() returned error code "   << procError   << STUFF ); \
    if( retAPIError ) FAIL( retAPIError, PROCNAME_UPDATE << "() failed with API error " << retAPIError << STUFF );

class D128Session
{
	private:
		int         mSessionNumber;
	
	public:
		std::string mError;
	
		D128Session() : mSessionNumber( 0 ) {}
		~D128Session() { Close(); }
		int Open( void )
		{
			if( mSessionNumber ) return mSessionNumber;
			mError = "";
			if( !D128_Init || !D128_Update || !D128_Close )
			{
				HINSTANCE dll; 
				const char * dllName = "D128API.DLL";
                if( ( dll = LoadLibraryA( dllName ) ) == NULL ) FAIL( 0, "failed to load " << dllName );
				if( ( D128_Init   = ( PROCTYPE_INIT   )GetProcAddress( dll, PROCNAME_INIT   ) ) == NULL ) FAIL( 0, "failed to load " << PROCNAME_INIT   << " from " << dllName );
				if( ( D128_Update = ( PROCTYPE_UPDATE )GetProcAddress( dll, PROCNAME_UPDATE ) ) == NULL ) FAIL( 0, "failed to load " << PROCNAME_UPDATE << " from " << dllName );
				if( ( D128_Close  = ( PROCTYPE_CLOSE  )GetProcAddress( dll, PROCNAME_CLOSE  ) ) == NULL ) FAIL( 0, "failed to load " << PROCNAME_CLOSE  << " from " << dllName );
			}
			int procError = 0, retAPIError = 0;
			procError = D128_Init( &mSessionNumber, &retAPIError, NULL, NULL );
			if( procError   ) FAIL( 0, PROCNAME_INIT << "() returned error code "   << procError   );
			if( retAPIError ) FAIL( 0, PROCNAME_INIT << "() failed with API error " << retAPIError );
			return mSessionNumber;
		}
		void Close( void )
		{
			int procError = 0, retAPIError = 0;
			if( mSessionNumber && D128_Close ) procError = D128_Close( &mSessionNumber, &retAPIError, NULL, NULL );
			mSessionNumber = 0;
		}
};
D128Session SESSION;

class D128Device
{
    public:
	    DEVHDR mHeader;            // First two members must be DEVHDR
    	D128DEVICESTATE mDesired;  // and D128DEVICESTATE, corresponding to the definition of struct _D128  (but with space allocated for one device)
    
        D128DEVICESTATE * mLastKnown;
        std::string mError;
    private:
        int    mAllInfoSize;
        D128 * mAllInfo;
    
    public:
        D128Device( int deviceIndex = -1 )
        : mAllInfoSize( 0 ), mAllInfo( NULL ), mLastKnown( NULL )
        {
            mHeader.DeviceCount = 1;
            mDesired.D128_DeviceID = 0;
            if( deviceIndex >= 0 ) SelectDeviceByIndex( deviceIndex );
            BlankSlate();
        }
        ~D128Device() { CleanUp(); }
        void CleanUp( void )
        {
            if( mAllInfo ) free( mAllInfo );
            mAllInfo = NULL;
        }
        int Update( bool send=false )
        {
			int sessionReference = SESSION.Open();
            if( !sessionReference ) FAIL( -1, SESSION.mError );
            
            int procError = 0, retAPIError = 0, nBytesToReceive = 0, nBytesToSend = 0;
		    procError = D128_Update( sessionReference, &retAPIError, NULL, 0, NULL, &nBytesToReceive, NULL, NULL );
            CHECK( " while querying size of state information" );
            if( send ) nBytesToSend = ( int )( ( char * )( &mDesired + 1 ) - ( char * )&mHeader );
            if( !mAllInfo ) mAllInfo = ( D128 * )malloc( mAllInfoSize = nBytesToReceive * 10 );
            do
            {
                if( nBytesToReceive > mAllInfoSize ) mAllInfo = ( D128 * )realloc( mAllInfo, mAllInfoSize = nBytesToReceive );
                procError = D128_Update( sessionReference, &retAPIError, ( send ? ( D128 * )this : NULL ), nBytesToSend, mAllInfo, &nBytesToReceive, NULL, NULL );
            } while( nBytesToReceive > mAllInfoSize );
            CHECK( " while" << ( send ? " sending and " : " " ) << "querying device states" );
            if( mDesired.D128_DeviceID ) SelectDeviceBySerialNumber( mDesired.D128_DeviceID );
            else SelectDeviceByIndex( 0 ); // this will set mDesired.D128_DeviceID for next time (so it won't matter if the devices are returned in a different order next time)
            if( send ) BlankSlate();
            return 0;
        }
        int GetNumberOfDevices( void )
        {
            if( !mAllInfo && Update( false ) != 0 ) return 0;
            return mAllInfo->Header.DeviceCount;
        }
        int SelectDeviceByIndex( int deviceIndex )
        {
            if( !mAllInfo && Update( false ) != 0 ) return 0;
            if( deviceIndex >= mAllInfo->Header.DeviceCount ) FAIL( 0, "failed to find a device at index " << deviceIndex << " (device count = " << mAllInfo->Header.DeviceCount << ")" );
            mLastKnown = &mAllInfo->State[ deviceIndex ];
            mDesired = *mLastKnown;
            return mDesired.D128_DeviceID;
        }
        int SelectDeviceBySerialNumber( int serialNumber )
        {
            if( !mAllInfo && Update( false ) != 0 ) return 0;
            for( int deviceIndex = 0; deviceIndex < mAllInfo->Header.DeviceCount; deviceIndex++ )
            {
                if( mAllInfo->State[ deviceIndex ].D128_DeviceID == serialNumber )
                {
                    mLastKnown = &mAllInfo->State[ deviceIndex ];
                    mDesired = *mLastKnown;
                    return mDesired.D128_DeviceID;
                }
            }
            FAIL( 0, "failed to find a device with serial number " << serialNumber << " (device count = " << mAllInfo->Header.DeviceCount << ")" );
        }
        D128STATE & Slate( void )
        {
            return mDesired.D128_State;
        }
        D128STATE & BlankSlate( void )
        {
            if( mLastKnown ) mDesired = *mLastKnown;
            mDesired.D128_VersionID = -1;
            mDesired.D128_Error = -1;
            //mDesired.D128_State.CONTROL.ENABLE = -1;   // despite what the manual says, we cannot set this to -1 (-> beep + warning triangle appears in first status icon position)
            //mDesired.D128_State.CONTROL.MODE = -1;     // despite what the manual says, we cannot set this to -1 (-> error code 100013)
            //mDesired.D128_State.CONTROL.POLARITY = -1; // despite what the manual says, we cannot set this to -1 (-> error code 100013)
            //mDesired.D128_State.CONTROL.SOURCE = -1;   // despite what the manual says, we cannot set this to -1 (-> error code 100013)
            mDesired.D128_State.CONTROL.ZERO = -1;
            mDesired.D128_State.CONTROL.TRIGGER = -1;
            mDesired.D128_State.CONTROL.NOBUZZER = -1;
            mDesired.D128_State.DEMAND = -1;
            mDesired.D128_State.WIDTH = -1;
            mDesired.D128_State.RECOVERY = -1;
            mDesired.D128_State.DWELL = -1;
            mDesired.D128_State.CPULSE = 0;
            mDesired.D128_State.COOC = 0;
            mDesired.D128_State.CTOOFAST = 0;
            mDesired.D128_State.FSTATE.OVERENERGY = 0;
            mDesired.D128_State.FSTATE.HWERROR = 0;
            return mDesired.D128_State;
        }
        const char * Error( void )
        {
            return mError.length() ? mError.c_str() : NULL;
        }
        
};

#define INTERPRET( FIELD )   if( cmd == #FIELD ) { stimulator.Slate().FIELD = arg; /* std::cout << #FIELD << " = " << stimulator.Slate().FIELD << std::endl; */ }
int main( int argc, const char * argv[] )
{
#if 1
    D128Device stimulator( 0 );
	if( stimulator.Error() ) { std::cerr << stimulator.Error() << std::endl; return -1; }
    unsigned char * firmwareVersion = ( unsigned char * )&stimulator.mLastKnown->D128_VersionID;
    std::cout << "   Serial number: " << stimulator.mLastKnown->D128_DeviceID << std::endl;
    std::cout << "Firmware version: " << ( int )firmwareVersion[ 3 ] << "." << ( int )firmwareVersion[ 2 ] << "." << ( int )firmwareVersion[ 1 ] << "." << ( int )firmwareVersion[ 0 ] << std::endl;
	while( true )
	{
	    if( stimulator.Error() ) { std::cerr << stimulator.Error() << std::endl; return -1; }
	    //REPORT( stimulator.mLastKnown->D128_State.DEMAND );
	    //REPORT( stimulator.Slate().DEMAND );
		
		std::string cmd, argstr, remainder;
		std::getline( std::cin, cmd );
		if( std::cin.fail() ) return 0;
		for( int i = 0; i < cmd.length(); i++ )
		{
			cmd[ i ] = ( cmd[ i ] == '=' ) ? ' '
			         : ( cmd[ i ] == '_' ) ? '.'
			         : std::toupper( cmd[ i ] );
		}

		std::stringstream ss( cmd );
		cmd = "";
		ss >> cmd >> argstr;
		if( cmd == "" ) continue;
		if( cmd == "EXIT" ) break;
		if( cmd == "UPDATE" )
		{
			stimulator.Update( true );
		}
		else
		{
			if( ss.fail() ) { std::cerr << "failed to interpret line \"" << ss.str() << "\""; return -1; }
			std::stringstream ss2( argstr );
			int arg;
			char zero, x;
			if( argstr.length() > 2 && argstr[ 0 ] == '0' && argstr[ 1 ] == 'x' ) ss2 >> zero >> x >> std::hex >> arg >> argstr;
			else ss2 >> arg >> remainder;
			if( remainder.length() ) { std::cerr << "failed to interpret value \"" << ss2.str() << "\""; return -1; }
			
			if( cmd == "SLEEP" ) ::Sleep( arg ); // milliseconds
			else INTERPRET( CONTROL.ENABLE   ) // ?   manual says: { 1 = disabled; 2 = enabled; -1 = leave as-is }; but empirically: { 0 = disabled; 1 = enabled; anything else: error icon on stimulator screen } 
			else INTERPRET( CONTROL.MODE     ) // ?A  1 = monophasic; 2 = biphasic
			else INTERPRET( CONTROL.POLARITY ) // ?A  1 = positive;   2 = negative; 3 = alternating
			else INTERPRET( CONTROL.SOURCE   ) // ?   1 = specify DEMAND via panel/API;  2 = specify DEMAND via external analog BNC input
			else INTERPRET( CONTROL.ZERO     ) //     1 = initiates zeroing of the external analog BNC input
			else INTERPRET( CONTROL.TRIGGER  ) //     1 = initiates a trigger (but this is not the most precisely-timed way of doing it: should use a TTL pulse to the trigger BNC instead)
			else INTERPRET( CONTROL.NOBUZZER ) //  A  0 = enable buzzer for out-of-compliance events; 1 = disable buzzer for out-of-compliance events; 
			else INTERPRET( DEMAND           ) //  A  set this to 10 * (desired current in mA)
			else INTERPRET( WIDTH            ) //  A  stimulus pulse duration in microseconds (50 to 2000)
			else INTERPRET( RECOVERY         ) //  A  in biphasic mode, specifies the recovery pulse amplitude as a percentage (10 to 100) of the stimulus pulse amplitude
			else INTERPRET( DWELL            ) //  A  time between end of the stimulus pulse and start of recovery pulse in microseconds (1 to 990)
			else { std::cerr << "unrecognized command/field \"" << cmd << "\""; return -1; }
		}
	}
	return 0;
#else    
    D128Device stimulator( 0 );
    if( stimulator.Error() ) { std::cerr << stimulator.Error() << std::endl; return -1; }
    stimulator.Slate().CONTROL.ENABLE = 0;
    if( stimulator.Update( true ) != 0 ) { std::cerr << stimulator.Error() << std::endl; return -1; }

    stimulator.Slate().CONTROL.SOURCE = 1;
    stimulator.Slate().RECOVERY = 100;
    stimulator.Slate().DWELL = 1;
    stimulator.Slate().CONTROL.POLARITY = 2;
    stimulator.Slate().CONTROL.MODE = 2;
    stimulator.Slate().WIDTH = 1000;
    stimulator.Slate().DEMAND = 22; // NB: cannot seem to change this via the API if the stimulator is armed when we call Update (even though you can via the knob)
    stimulator.Slate().CONTROL.ENABLE = 1; // TODO: this seems to toggle on and off for some reason (enabled-states must be separated by sufficient time?)
    if( stimulator.Update( true ) != 0 ) { std::cerr << stimulator.Error() << std::endl; return -1; }
        
    REPORT( stimulator.mLastKnown->D128_State.DEMAND );
    return 0;
#endif
}

/*
	infoPtr->Header.DeviceCount                          (int)            //
	infoPtr->State[iDevice].D128_DeviceID                (int)            //     device serial number (presumably use this to verify that you're addressing the same device each time)
	infoPtr->State[iDevice].D128_VersionID               (int)            //     device firmware version
	infoPtr->State[iDevice].D128_Error                   (int)            //     
	infoPtr->State[iDevice].D128_State.CONTROL.ENABLE    (int:2)          // ?   manual says: { 1 = disabled; 2 = enabled; -1 = leave as-is }; but empirically: { 0 = disabled; 1 = enabled; anything else: error icon on stimulator screen } 
	infoPtr->State[iDevice].D128_State.CONTROL.MODE      (int:3)          // ?A  1 = monophasic; 2 = biphasic
	infoPtr->State[iDevice].D128_State.CONTROL.POLARITY  (int:3)          // ?A  1 = positive;   2 = negative; 3 = alternating
	infoPtr->State[iDevice].D128_State.CONTROL.SOURCE    (int:3)          // ?   1 = specify DEMAND via panel/API;  2 = specify DEMAND via external analog BNC input
	infoPtr->State[iDevice].D128_State.CONTROL.ZERO      (int:2)          //     1 = initiates zeroing of the external analog BNC input
	infoPtr->State[iDevice].D128_State.CONTROL.TRIGGER   (int:2)          //     1 = initiates a trigger (but this is not the most precisely-timed way of doing it: should use a TTL pulse to the trigger BNC instead)
	infoPtr->State[iDevice].D128_State.CONTROL.NOBUZZER  (int:2)          //  A  0 = enable buzzer for out-of-compliance events; 1 = disable buzzer for out-of-compliance events; 
	infoPtr->State[iDevice].D128_State.DEMAND            (int)            //  A  set this to 10 * (desired current in mA)
	infoPtr->State[iDevice].D128_State.WIDTH             (int)            //  A  stimulus pulse duration in microseconds (50 to 2000)
	infoPtr->State[iDevice].D128_State.RECOVERY          (int)            //  A  in biphasic mode, specifies the recovery pulse amplitude as a percentage (10 to 100) of the stimulus pulse amplitude
	infoPtr->State[iDevice].D128_State.DWELL             (int)            //  A  time between end of the stimulus pulse and start of recovery pulse in microseconds (1 to 990)
	infoPtr->State[iDevice].D128_State.CPULSE            (unsigned int)   //     number of stimulus pulses delivered since last enabled
	infoPtr->State[iDevice].D128_State.COOC              (unsigned int)   //     number of out-of-complicance events since last enabled
	infoPtr->State[iDevice].D128_State.CTOOFAST          (unsigned int)   //     number of trigger-too-fast events since last enabled
	infoPtr->State[iDevice].D128_State.FSTATE.OVERENERGY (unsigned int:1) //     
	infoPtr->State[iDevice].D128_State.FSTATE.HWERROR    (unsigned int:1) //     

	// Fields marked with an "A" are set in Amir's DS8library.cpp implementation
	
	// TODO:
	// - You're supposed to be able to set a value of -1 for any of the signed ints in infoPtr->State[iDevice].D128_State and infoPtr->State[iDevice].D128_State.CONTROL
	//   if you mean "no change" but the ones marked '?' don't seem to obey that---see also BlankSlate()
    //   This problem seems to happen with DLL 1.5.0.0 from 2020-11-19, DLL 1.4.3.7 from 2019-07-17  and DLL 1.2.0.0 from 2017-07-08
    //   It might be firmware-version-dependent - the problem happens with stimulator S/N 80, FW 1.4.3.0, but not with the main lab stimulator (whatever version that has)
	// - Note the discrepancy between what the manual says and the actual mapping of values to behavior in CONTROL.ENABLE (doesn't seem API-version-dependent or stimulator-dependent)
	// - Do we need to / how do we obtain an updated D128API.h ?  Probably just fills in a couple of missing error code definitions, nothing more.
    // - Should we just switch to D128Proxy.dll ?   That seems to have a much simpler API.  Does it have the same quirks (above)?
    // - Cannot change DEMAND while CONTROL.ENABLE is on (is that true of all units?)
    //   You can turn off CONTROL.ENABLE in one update, then simultaneously change DEMAND and turn CONTROL.ENABLE back on in the next update, but you must leave at least 70 msec pause between successive "enabled" states.
*/
