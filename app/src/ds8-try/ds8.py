__all__ = [
	'DS8R',
]

class CONSTANTS:
	
	# control_enable values:
	OFF = ENABLED  = 0
	ON  = DISABLED = 1
	
	# control_mode values:
	MONOPHASIC = 1
	BIPHASIC   = 2

	# control_polarity values:
	POSITIVE    = 1
	NEGATIVE    = 2
	ALTERNATING = 3

	# control_source values:
	INTERNAL = 1
	EXTERNAL = 2

	# control_nobuzzer values:
	AUDIBLE = 0
	SILENT  = 1

for name, value in CONSTANTS.__dict__.items():
	if name.startswith( '_' ): continue
	globals()[ name ] = value
	__all__.append( name )


import os
import sys
import subprocess


HERE = os.path.dirname( __file__ )

if sys.version_info[ 0 ] < 3: bytes = str
else: unicode = str
	
class DigitimerException( Exception ): pass

def DS8R( *pargs, **kwargs ):
	"""
	Ordered arguments `*pargs`, if supplied, are taken first: each
	one should be a string expressing a command of the form
	`'KEY VALUE'`. The one exception is the `'UPDATE'` command which
	has no `VALUE`.
	
	Keyword arguments `**kwargs` are a more-convenient interface
	to the same command set, but you cannot guarantee the order in
	which they are carried out. Keys are case-insensitive, and
	underscores can be used instead of dots.
	
	Example::
	
		DS8R( demand=26, control_mode=BIPHASIC, control_polarity=POSITIVE, control_source=INTERNAL, control_enable=ON )
	
	The underlying command set is as follows::

		CONTROL.ENABLE     the manual says: { 1 = disabled; 2 = enabled; -1 = leave as-is }; however, empirically it is: { 0 = disabled; 1 = enabled; anything else: error icon on stimulator screen } 
		CONTROL.MODE       1 = monophasic; 2 = biphasic
		CONTROL.POLARITY   1 = positive;   2 = negative; 3 = alternating
		CONTROL.SOURCE     1 = specify DEMAND via panel/API;  2 = specify DEMAND via external analog BNC input
		CONTROL.ZERO       1 = initiates zeroing of the external analog BNC input
		CONTROL.TRIGGER    1 = initiates a trigger (but note that this is not the most precisely-timed way of doing it: should use a TTL pulse to the trigger BNC instead)
		CONTROL.NOBUZZER   0 = enable buzzer for out-of-compliance events; 1 = silence the buzzer for out-of-compliance events; 
		DEMAND             set this to 10 * (desired current in mA)
		WIDTH              stimulus pulse duration in microseconds (50 to 2000)
		RECOVERY           in biphasic mode, specifies the recovery pulse amplitude as a percentage (10 to 100) of the stimulus pulse amplitude
		DWELL              in biphasic mode, specifies the time between end of the stimulus pulse and start of recovery pulse in microseconds (1 to 990)

	An example use-case for ordered arguments would be that, on some
	stimulators but not others (and I'm not sure whether this is
	dependent on hardware or firmware version), you cannot change
	DEMAND while the stimulator is enabled. So, to work around this,
	you can say::
		
		DS8R(
			'CONTROL.ENABLE 0',
			'UPDATE',
			'SLEEP 100',  # must leave at least 90ms pause between sucessive CONTROL.ENABLED=1 states
			'DEMAND 25',  # or 10 * whatever number of mA you want
			'CONTROL.ENABLE 1',
		)

	This function always appends an UPDATE command at the end of
	your complete set of commands (ordered and keyword args, taken
	together).

	"""
	sp = subprocess.Popen( [ os.path.join( HERE, 'ds8.exe' ) ], shell=False, stdin=subprocess.PIPE, stdout=subprocess.PIPE, stderr=subprocess.PIPE, universal_newlines=True )
	#if 'demand' in kwargs: kwargs[ 'demand' ] = int( round( 10 * kwargs[ 'demand' ] ) )
	cmds = '\n'.join( list( pargs ) + [ '%s = %d' % ( k.upper().replace( '_', '.' ), v ) for k, v in kwargs.items() ] )
	if cmds: cmds += '\nUPDATE\n'
	out, err = sp.communicate( cmds )
	sp.wait()
	#if str is not bytes: out, err = out.decode( 'utf-8' ), err.decode( 'utf-8' )
	if sp.returncode: raise DigitimerException( err )
	return out
