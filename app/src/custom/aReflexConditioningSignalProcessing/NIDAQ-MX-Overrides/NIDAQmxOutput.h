#ifndef   __NIDAQmx_OUTPUT_H__
#define   __NIDAQmx_OUTPUT_H__

#include <string>

#include "NIDAQmx.imports.h"
#include "Environment.h"
#include "Expression.h"

class NIDAQmxOutputHandler : public Environment
{
# ifndef OUTPUTHANDLER_PREFLIGHT_CONST
#   define OUTPUTHANDLER_PREFLIGHT_CONST 1
# endif
# if OUTPUTHANDLER_PREFLIGHT_CONST
#   define OUTPUTHANDLER_PREFLIGHT_BEHAVIOR const
#   define OUTPUTHANDLER_MEMBER_QUALIFIER
# else
#   define OUTPUTHANDLER_PREFLIGHT_BEHAVIOR
#   define OUTPUTHANDLER_MEMBER_QUALIFIER mutable
# endif
  public:
    NIDAQmxOutputHandler();
    ~NIDAQmxOutputHandler();
   
    void OnPublish( const std::string & section, const std::string & parameterName );
    void OnPreflight(  const SignalProperties & inputSignalProperties ) OUTPUTHANDLER_PREFLIGHT_BEHAVIOR;
    void OnInitialize( const SignalProperties & inputSignalProperties );
    void OnHalt( void );
    void OnStartRun( void );
    void OnStopRun( void );
    void OnProcess( const GenericSignal & inputSignal );
   
    void ConfigureOutputs(
      const SignalProperties & rInputSignalProperties,
      TaskHandle & rDigitalOutputTaskHandle,
      TaskHandle & rAnalogOutputTaskHandle,
      Expression::VariableContainer & rOutputExpressionVariables,
      std::vector< Expression > & rOutputInitializationExpressions,
      std::vector< Expression > & rDigitalOutputExpressions,
      std::vector< Expression > & rAnalogOutputExpressions,
      uInt32 & rAnalogBatchSize
    ) const;
  
  private:
    std::string mParameterName;
  
     TaskHandle                    mDigitalOutputTaskHandle;
     TaskHandle                    mAnalogOutputTaskHandle;
     Expression::VariableContainer mOutputExpressionVariables;
     std::vector<Expression>       mDigitalOutputExpressions;
     std::vector<Expression>       mAnalogOutputExpressions;
     std::vector<Expression>       mOutputInitializationExpressions;
     uInt32                        mAnalogBatchSize;
     uInt8   *                     mDigitalOutputBuffer;
     float64 *                     mAnalogOutputBuffer;
};


#endif /* __NIDAQmx_OUTPUT_H__ */
