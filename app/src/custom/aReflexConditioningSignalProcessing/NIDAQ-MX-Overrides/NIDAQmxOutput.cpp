#ifndef   __NIDAQmx_OUTPUT_CPP__
#define   __NIDAQmx_OUTPUT_CPP__

#include "NIDAQmxOutput.h"

int32 CheckDAQmxFailure( int32 resultCode )
{
  if( !DAQmxFailed( resultCode ) ) return 0;
  static char errorMessageBuffer[ 2048 ];
  sprintf( errorMessageBuffer, "function returned error code %d", resultCode );  /* fallback in case DAQmxGetExtendedErrorInfo() fails */
  DAQmxGetExtendedErrorInfo( errorMessageBuffer, sizeof( errorMessageBuffer ) );
  bcierr << "error from DAQmx API: " << errorMessageBuffer;
  return resultCode;
}


NIDAQmxOutputHandler::NIDAQmxOutputHandler()
: mDigitalOutputTaskHandle( NULL ),
  mDigitalOutputBuffer( NULL ),
  mAnalogOutputTaskHandle( NULL ),
  mAnalogOutputBuffer( NULL )
{
  
}

NIDAQmxOutputHandler::~NIDAQmxOutputHandler()
{
  
}

void
NIDAQmxOutputHandler::OnPublish( const std::string & section, const std::string & parameterName )
{
  mParameterName = parameterName;
  std::string s = section + " matrix " + parameterName + "= 1 { Analog%20or%20Digital DAQmx%20Channel%20Spec Expression AnalogMin AnalogMax } //";
  BEGIN_PARAMETER_DEFINITIONS
    s.c_str()
  END_PARAMETER_DEFINITIONS
}

void
NIDAQmxOutputHandler::ConfigureOutputs(
  const SignalProperties & rInputSignalProperties,
  TaskHandle & rDigitalOutputTaskHandle,
  TaskHandle & rAnalogOutputTaskHandle,
  Expression::VariableContainer & rOutputExpressionVariables,
  std::vector< Expression > & rOutputInitializationExpressions,
  std::vector< Expression > & rDigitalOutputExpressions,
  std::vector< Expression > & rAnalogOutputExpressions,
  uInt32 & rAnalogBatchSize
) const
{  
  //bciwarn << "setting up NIDAQmx outputs";
  rDigitalOutputTaskHandle = NULL;
  rAnalogOutputTaskHandle = NULL;
  rOutputExpressionVariables.clear();
  rOutputInitializationExpressions.clear();
  rDigitalOutputExpressions.clear();
  rAnalogOutputExpressions.clear();
  
  rAnalogBatchSize = 1; // TODO: set it to 1 to use analog outputs in "on-demand" mode just like digital (but this seems to silently fail to produce output, so far).
  //rAnalogBatchSize = Parameter( "SampleBlockSize" ); // TODO: set it to SampleBlockSize to attempt continuous sample-by-sample control at the main SamplingRate (fails so far, with serious runtime problems)
  
  std::stringstream ss;
  ss << "Dev" << ( int )OptionalParameter( "BoardNumber", 1 );
  std::string defaultDeviceName = ss.str();
  
  ParamRef matrix = Parameter( mParameterName );
  if( matrix->NumRows() > 0 && matrix->NumColumns() != 5 ) throw bcierr << mParameterName << " parameter should have 5 columns"; // TODO: implement NIDAQFilter backward compatibility mode 
  for( int iRow = 0; iRow < matrix->NumRows(); iRow++ )
  {
    int iColumn = 0;
    std::string type = matrix( iRow, iColumn++ );
    std::string spec = matrix( iRow, iColumn++ );
    std::string estr = matrix( iRow, iColumn++ );
    if( !type.length() && !spec.length() && !estr.length() ) continue;
    if( !estr.length() ) throw bcierr << "error in row " << ( iRow + 1 ) << " of " << mParameterName << ": expression (column 3) cannot be blank";
    Expression expr( estr );
    
    std::string deviceName = defaultDeviceName;
    if( StringUtils::ToLower( spec ).substr( 0, 3 ) == "dev" )
    {
      std::vector< std::string > parts = StringUtils::Split( "/", spec );
      deviceName = parts[ 0 ];
    }
    else spec = deviceName + "/" + spec;
    
    type = StringUtils::ToLower( type );
    if( type == "digital" )
    {
      if( !spec.length() ) throw bcierr << "error in row " << ( iRow + 1 ) << " of " << mParameterName << ": DAQmx channel spec (column 2) cannot be blank";
      rDigitalOutputExpressions.push_back( expr );
      if( !rDigitalOutputTaskHandle && CheckDAQmxFailure( DAQmxCreateTask( "DigitalOutput", &rDigitalOutputTaskHandle ) ) ) return;
      if( CheckDAQmxFailure( DAQmxCreateDOChan( rDigitalOutputTaskHandle, spec.c_str(), "", DAQmx_Val_ChanForAllLines ) ) ) return;
    }
    else if( type == "analog" )
    {
      if( !spec.length() ) throw bcierr << "error in row " << ( iRow + 1 ) << " of " << mParameterName << ": DAQmx channel spec (column 2) cannot be blank";
      rAnalogOutputExpressions.push_back( expr );
      if( !rAnalogOutputTaskHandle && CheckDAQmxFailure( DAQmxCreateTask( "AnalogOutput", &rAnalogOutputTaskHandle ) ) ) return;
      double minVolts = matrix( iRow, iColumn++ ).InVolts();
      double maxVolts = matrix( iRow, iColumn++ ).InVolts();
      if( CheckDAQmxFailure( DAQmxCreateAOVoltageChan( rAnalogOutputTaskHandle, spec.c_str(), "", minVolts, maxVolts, DAQmx_Val_Volts, NULL ) ) ) return;
      bool32  sampleClockSupported;
      if( rAnalogBatchSize > 1 && CheckDAQmxFailure( DAQmxGetDevAOSampClkSupported( deviceName.c_str(), &sampleClockSupported ) ) || !sampleClockSupported )
      {
        bciwarn << deviceName << " does not appear to support custom sampling rates for analog output";
        rAnalogBatchSize = 1;
      }
    }
    else if( type == "initial" )
    {
      rOutputInitializationExpressions.push_back( expr );
    }
    else throw bcierr << "error in row " << ( iRow + 1 ) << " of " << mParameterName << ": first column must be \"analog\" or \"digital\" or \"initial\"";
  }
  GenericSignal signal( rInputSignalProperties );
  for( auto expr = rOutputInitializationExpressions.begin(); expr != rOutputInitializationExpressions.end(); expr++ ) { expr->Compile( rOutputExpressionVariables ); expr->Evaluate(); }
  for( auto expr = rDigitalOutputExpressions.begin();        expr != rDigitalOutputExpressions.end();        expr++ ) { expr->Compile( rOutputExpressionVariables ); expr->Evaluate( &signal ); }
  for( auto expr = rAnalogOutputExpressions.begin();         expr != rAnalogOutputExpressions.end();         expr++ )
  {
    expr->Compile( rOutputExpressionVariables );
    for( int iSample = 0; iSample < rAnalogBatchSize; iSample++ ) expr->Evaluate( &signal, iSample );
  }
  if( rAnalogOutputExpressions.size() && rAnalogBatchSize > 1 )
  {
    if( CheckDAQmxFailure( DAQmxCfgSampClkTiming( rAnalogOutputTaskHandle, "ao/SampleClockTimebase", Parameter( "SamplingRate" ), DAQmx_Val_Rising, DAQmx_Val_ContSamps, rAnalogBatchSize ) ) ) return;
    if( CheckDAQmxFailure( DAQmxSetBufOutputBufSize( rAnalogOutputTaskHandle, rAnalogBatchSize ) ) ) return;
  }
}

void
NIDAQmxOutputHandler::OnPreflight( const SignalProperties & inputSignalProperties ) OUTPUTHANDLER_PREFLIGHT_BEHAVIOR
{
  State( "Running" );	
# if OUTPUTHANDLER_PREFLIGHT_CONST // strict BCI2000 behavior (do all the initialization, then tear it down, then do it again for real)
    TaskHandle digitalOutputTaskHandle;
    TaskHandle analogOutputTaskHandle;
    Expression::VariableContainer outputExpressionVariables;
    std::vector< Expression > outputInitializationExpressions;
    std::vector< Expression > digitalOutputExpressions;
    std::vector< Expression > analogOutputExpressions;
    uInt32 analogBatchSize;
    ConfigureOutputs( inputSignalProperties,  digitalOutputTaskHandle,  analogOutputTaskHandle,  outputExpressionVariables,  outputInitializationExpressions,  digitalOutputExpressions,  analogOutputExpressions,  analogBatchSize );
    if( digitalOutputTaskHandle ) DAQmxClearTask( digitalOutputTaskHandle );
    if( analogOutputTaskHandle  ) DAQmxClearTask( analogOutputTaskHandle );
# else // assume this object was declared as `mutable` in the parent class, and only do the DAQmx initialization once:
    // TODO: although this worked once, it no longer does (no output gets triggered)
    ConfigureOutputs( inputSignalProperties, mDigitalOutputTaskHandle, mAnalogOutputTaskHandle, mOutputExpressionVariables, mOutputInitializationExpressions, mDigitalOutputExpressions, mAnalogOutputExpressions, mAnalogBatchSize );
# endif
}

void
NIDAQmxOutputHandler::OnInitialize( const SignalProperties & inputSignalProperties )
{
# if OUTPUTHANDLER_PREFLIGHT_CONST
    ConfigureOutputs( inputSignalProperties, mDigitalOutputTaskHandle, mAnalogOutputTaskHandle, mOutputExpressionVariables, mOutputInitializationExpressions, mDigitalOutputExpressions, mAnalogOutputExpressions, mAnalogBatchSize );
# endif
  mDigitalOutputBuffer = mDigitalOutputExpressions.size() ? new uInt8[ mDigitalOutputExpressions.size() ] : NULL;
  mAnalogOutputBuffer  = mAnalogOutputExpressions.size()  ? new float64[ mAnalogOutputExpressions.size() * mAnalogBatchSize ] : NULL;
}

void
NIDAQmxOutputHandler::OnHalt( void )
{
  if( mDigitalOutputTaskHandle ) DAQmxClearTask( mDigitalOutputTaskHandle );  mDigitalOutputTaskHandle = NULL;
  delete [] mDigitalOutputBuffer; mDigitalOutputBuffer = NULL;
  if( mAnalogOutputTaskHandle ) DAQmxClearTask( mAnalogOutputTaskHandle );  mAnalogOutputTaskHandle = NULL;
  delete [] mAnalogOutputBuffer; mAnalogOutputBuffer = NULL;
}

void
NIDAQmxOutputHandler::OnStartRun( void )
{
  if( mAnalogOutputTaskHandle && mAnalogBatchSize > 1 ) 
  {
    for( int i = 0; i < mAnalogBatchSize * mAnalogOutputExpressions.size(); i++ ) mAnalogOutputBuffer[ i ] = 0.0;
    int32 nSamplesPerChannelWritten;
    CheckDAQmxFailure( DAQmxWriteAnalogF64(
      mAnalogOutputTaskHandle,       // taskHandle
      mAnalogBatchSize,              // numSampsPerChan
      false,                         // autoStart
      1.0,                           // timeout  (seconds)
      DAQmx_Val_GroupByScanNumber,   // dataLayout  DAQmx_Val_GroupByChannel (noninterleaved) or DAQmx_Val_GroupByScanNumber (interleaved)
      mAnalogOutputBuffer,           // writeArray  ( float64 [] )
      &nSamplesPerChannelWritten,    // sampsPerChanWritten ( int32 * )
      NULL                           // reserved (must be NULL)
    ) ); // TODO: this avoids API errors, but the DAQmxWriteAnalogF64 calls seem to hang or time-out at run-time
  }
  if( mDigitalOutputTaskHandle ) CheckDAQmxFailure( DAQmxStartTask( mDigitalOutputTaskHandle ) );
  if( mAnalogOutputTaskHandle  ) CheckDAQmxFailure( DAQmxStartTask( mAnalogOutputTaskHandle  ) );
  mOutputExpressionVariables.clear();
  for( auto expr = mOutputInitializationExpressions.begin(); expr != mOutputInitializationExpressions.end(); expr++ ) expr->Evaluate();
}

void
NIDAQmxOutputHandler::OnStopRun( void )
{
  if( mDigitalOutputTaskHandle ) CheckDAQmxFailure( DAQmxStopTask( mDigitalOutputTaskHandle ) );
  if( mAnalogOutputTaskHandle  ) CheckDAQmxFailure( DAQmxStopTask( mAnalogOutputTaskHandle  ) );
}

void
NIDAQmxOutputHandler::OnProcess( const GenericSignal & inputSignal )
{
  if( State( "Running" ) == 0 ) return;
	
  if( mDigitalOutputExpressions.size() )
  {
    uInt8 * vals = mDigitalOutputBuffer;
    for( auto expr = mDigitalOutputExpressions.begin(); expr != mDigitalOutputExpressions.end(); expr++ )
      *vals++ = expr->Evaluate( &inputSignal ); // NB: only evaluated on the first sample of the sample-block 
    int32 nSamplesPerChannelWritten;
    CheckDAQmxFailure( DAQmxWriteDigitalLines(
      mDigitalOutputTaskHandle,      // taskHandle
      1,                             // numSampsPerChan
      false,                         // autoStart
      1.0,                           // timeout  (seconds)
      DAQmx_Val_GroupByScanNumber,   // dataLayout  DAQmx_Val_GroupByChannel (noninterleaved) or DAQmx_Val_GroupByScanNumber (interleaved)
      mDigitalOutputBuffer,          // writeArray  ( uInt8 [] )
      &nSamplesPerChannelWritten,    // sampsPerChanWritten ( int32 * )
      NULL                           // reserved (must be NULL)
    ) );
  }
  if( mAnalogOutputExpressions.size() )
  {
    float64 * vals = mAnalogOutputBuffer;
    for( auto expr = mDigitalOutputExpressions.begin(); expr != mDigitalOutputExpressions.end(); expr++ )
      for( int iSample = 0; iSample < mAnalogBatchSize; iSample++ )
        *vals++ = expr->Evaluate( &inputSignal, iSample ); // either evaluated on only the first sample of the sampleBlock, or on each sample, depending on mAnalogBatchSize
    int32 nSamplesPerChannelWritten;
    CheckDAQmxFailure( DAQmxWriteAnalogF64(
      mAnalogOutputTaskHandle,       // taskHandle
      mAnalogBatchSize,              // numSampsPerChan
      false,                         // autoStart
      1.0,                           // timeout  (seconds)
      DAQmx_Val_GroupByScanNumber,   // dataLayout  DAQmx_Val_GroupByChannel (noninterleaved) or DAQmx_Val_GroupByScanNumber (interleaved)
      mAnalogOutputBuffer,           // writeArray  ( float64 [] )
      &nSamplesPerChannelWritten,    // sampsPerChanWritten ( int32 * )
      NULL                           // reserved (must be NULL)
    ) );
  }
}

#endif /* __NIDAQmx_OUTPUT_CPP__ */
