@echo off

set "OLDD=%CD%
set "HERE=%~dp0
cd /D "%HERE%"
where msbuild || call VisualStudio.cmd Win64

:: second argument could be e.g.  C:\neurotech\epocs\app

set  SECTION=Custom
set LOCATION=prog
set   TARGET=aReflexConditioningSignalProcessing
msbuild ..\..\..\build\BCI2000.sln /t:%SECTION%\%TARGET% /p:Configuration=Release /p:Platform=%VSCMD_ARG_TGT_ARCH%
if not "%1"=="" copy ..\..\..\%LOCATION%\%TARGET%*.exe "%1"\%LOCATION%\
:: NB: if CMakeCache.txt is newer than mid-2021, we'll need to transfer the vcredist DLLs as well

cd /D "%OLDD%"
