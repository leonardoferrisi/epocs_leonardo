// This is the main DLL file.

#include "DS8library.h"
#include "stdlib.h"

namespace DS8library
{

	DS8Functions::DS8Functions(void) {
		CurrentMax = 500;
		OutputState = FALSE;
		Initialise();	
	}

	DS8Functions::~DS8Functions(void) {
		
		CloseDS8();
		
	}

	void DS8Functions::Initialise(void){
	ErrorCode = 0;
	MODE = 0x02;
	POLARITY = 0x02;
	CURRENT = 20;
	WIDTH = 500;
	RECOVERY = 100;
	DWELL = 1;

	if (loadD128APILibrary()) {
		apiRef = 0;
		retError = procInitialise(&apiRef, &retAPIError, NULL, NULL);
		if ((retError != ERROR_SUCCESS) || (retAPIError != ERROR_SUCCESS)) {
			ErrorCode = -1;
		}
		else
		{
			ErrorCode = GetState();
			if (nDS8 > 0) {	SetVariables(NULL,NULL,NULL,NULL,NULL,NULL,TRUE); }
			else {ErrorCode = -1;}
		}
	}
	else 
	{
		ErrorCode = -1;
	}

	}

	//Initialize the variables needed for communicating with the DS8
	bool DS8Functions::loadD128APILibrary() {

		libD128API = LoadLibraryA("D128API.DLL");
		if (libD128API) {
			if (!(procInitialise = (DGD128_Initialise)GetProcAddress(libD128API, "DGD128_Initialise"))) {
				return FALSE;
			};

			if (!(procUpdate = (DGD128_Update)GetProcAddress(libD128API, "DGD128_Update"))) {
				return FALSE;
			};

			if (!(procClose = (DGD128_Close)GetProcAddress(libD128API, "DGD128_Close"))) {
				return FALSE;
			};

			return TRUE;

		}
		else {
			return FALSE;
		}
	}

	void DS8Functions::SetVariables(int mode, int polarity, int current, int width, int recovery, int dwell,bool Update)
	{
		//need error checking

		if ((mode == 1) | (mode == 2) ) {	MODE = mode; }

		if ((polarity >= 1) & (polarity <= 3)) { POLARITY = polarity; }
		
		if ((current >= 20 ) && (current <= CurrentMax)) { CURRENT = current; }
		if (current < 20 ) 
		{ CURRENT = 20; }
		if (current > CurrentMax) 
		{CURRENT = CurrentMax; }
		
		if (width != NULL) { WIDTH = width; }

		if (recovery != NULL) { RECOVERY = recovery; }
		
		if (dwell != NULL) { DWELL = dwell; }

		if ( Update )
		{
			UpdateDS8();
		}

	}

	int DS8Functions::GetState()
	{
		//Get SIZEOF current state (bytes) to be used for CurrentState
		retError = procUpdate(apiRef, &retAPIError, NULL, 0, NULL, &cbState, NULL, NULL);

		if ((retError == ERROR_SUCCESS) && (retAPIError == ERROR_SUCCESS)) {
			//allocate sufficient memory to retreive the device state data
			CurrentState = (PD128)malloc(cbState);
			retError = procUpdate(apiRef, &retAPIError, NULL, 0, CurrentState, &cbState, NULL, NULL);
			if ((retError == ERROR_SUCCESS) && (retAPIError == ERROR_SUCCESS)) {
				nDS8 = CurrentState->Header.DeviceCount;
			}
			else { return -1; }
		}
		else
		{
			return -1;
		}

		return 0;

	}

	//Enable/Diable the Output
	int DS8Functions::ToggleOutput(bool enable)
	{
		if (enable) { CurrentState->State[0].D128_State.CONTROL.ENABLE = 0x01; }
		else { CurrentState->State[0].D128_State.CONTROL.ENABLE = 0x00; }

		OutputState = enable;
		return UpdateDS8();


	}

	void DS8Functions::UpdateState()
	{
		CurrentState->State[0].D128_State.CONTROL.MODE = MODE;
		CurrentState->State[0].D128_State.RECOVERY = RECOVERY;
		CurrentState->State[0].D128_State.DWELL = DWELL;
		CurrentState->State[0].D128_State.WIDTH = WIDTH;
		CurrentState->State[0].D128_State.CONTROL.POLARITY = POLARITY;
		CurrentState->State[0].D128_State.DEMAND = CURRENT;
		if (CURRENT <= 20){
			CurrentState->State[0].D128_State.CONTROL.ENABLE = 0x00;
		} else {
			if (OutputState) {CurrentState->State[0].D128_State.CONTROL.ENABLE = 0x01;};
		}
	}


	int DS8Functions::UpdateDS8()
	{
		//GetState();
		UpdateState();
		procUpdate(apiRef, &retAPIError, CurrentState, cbState, CurrentState, &cbState, NULL, NULL);

		if ((retError != ERROR_SUCCESS) || (retAPIError != ERROR_SUCCESS)) {
			if (retError != ERROR_SUCCESS) { return retError; }
			else if (retAPIError != ERROR_SUCCESS) { return retAPIError; }
		}
		else {
			return 0;
		}

		return 0;
	}

	void DS8Functions::CloseDS8()
	{

		if (ErrorCode == 0){
			if (apiRef) {
				retError = procClose(&apiRef, &retAPIError, NULL, NULL);
			}
		}
	}




 
}