###########################################################################
## $Id:  $
## Authors: Amir Eftekhar (a.eftekhar@gmail.com)
## Description: Build information for the aReflexConditioningSignalProcessing project
##              
##              
###########################################################################

ADD_SUBDIRECTORY( cmdline )

IF( NOT WIN32 )
	RETURN()  # due to inclusion of DigitimerFilter, no hope of compiling on non-Windows
ENDIF( NOT WIN32 )

# Set the executable name
SET( EXECUTABLE_NAME aReflexConditioningSignalProcessing )

# Set the project specific sources
SET( SRC_PROJECT
  PipeDefinition.cpp        
  BackgroundTriggerFilter.cpp
  DigitimerFilter.cpp
  DS8library.cpp
  D188Library.cpp
  TrapFilter.cpp
  RangeIntegrator.cpp
  SharedMemoryOutputConnector.cpp
)

SET( HDR_PROJECT
  BackgroundTriggerFilter.h
  DigitimerFilter.h
  DS8library.h
  D188Library.h
  TrapFilter.h
  RangeIntegrator.h
  SharedMemoryOutputConnector.h
)

LIST( APPEND SRC_PROJECT ${BCI2000_SRC_DIR}/shared/modules/CoreMain.cpp )
BCI2000_ADD_SIGNAL_PROCESSING_MODULE( 
  "${EXECUTABLE_NAME}" 
  "${SRC_PROJECT}" "${HDR_PROJECT}" 
)
