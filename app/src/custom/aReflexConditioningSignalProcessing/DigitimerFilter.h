////////////////////////////////////////////////////////////////////////////////
// Authors: 
// Description: DigitimerFilter header
////////////////////////////////////////////////////////////////////////////////

#ifndef INCLUDED_DIGITIMERFILTER_H  // makes sure this header is not included more than once
#define INCLUDED_DIGITIMERFILTER_H

#include "GenericFilter.h"
#include "D188Library.h"
#include "DS8Library.h"

class DigitimerFilter : public GenericFilter
{
  public:
    DigitimerFilter();
    ~DigitimerFilter();
    void Publish() override;
    void Halt() override;
    void Preflight( const SignalProperties& inputProperties, SignalProperties& outputProperties ) const override;
    void Initialize( const SignalProperties& inputProperties, const SignalProperties& outputProperties ) override;
    void StartRun() override;
    void StopRun() override;
    void Process( const GenericSignal& inputSignal, GenericSignal& outputSignal ) override;
    
  private:
    D188library::D188Functions * mD188Controller;
    DS8library::DS8Functions   * mDS8Controller;

    int  mD188Channel;
};

#endif // INCLUDED_DIGITIMERFILTER_H