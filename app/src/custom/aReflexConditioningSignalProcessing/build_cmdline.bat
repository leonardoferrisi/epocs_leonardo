@echo off

set "OLDD=%CD%
set "HERE=%~dp0
cd /D "%HERE%"
where msbuild || call VisualStudio.cmd Win64

:: second argument could be e.g.  C:\neurotech\epocs\app

:: NB: if CMakeCache.txt is newer than mid-2021, do we need to transfer the vcredist DLLs as well?

set  SECTION=Core\Tools\cmdline
set LOCATION=tools\cmdline
set   TARGET=TransmissionFilter
msbuild ..\..\..\build\BCI2000.sln /t:%SECTION%\%TARGET% /p:Configuration=Release /p:Platform=%VSCMD_ARG_TGT_ARCH%
if not "%1"=="" copy ..\..\..\%LOCATION%\%TARGET%*.exe "%1"\%LOCATION%\
set   TARGET=SpatialFilter
msbuild ..\..\..\build\BCI2000.sln /t:%SECTION%\%TARGET% /p:Configuration=Release /p:Platform=%VSCMD_ARG_TGT_ARCH%
if not "%1"=="" copy ..\..\..\%LOCATION%\%TARGET%*.exe "%1"\%LOCATION%\
set   TARGET=IIRBandpass
msbuild ..\..\..\build\BCI2000.sln /t:%SECTION%\%TARGET% /p:Configuration=Release /p:Platform=%VSCMD_ARG_TGT_ARCH%
if not "%1"=="" copy ..\..\..\%LOCATION%\%TARGET%*.exe "%1"\%LOCATION%\

set  SECTION=Custom
set LOCATION=tools\cmdline
set   TARGET=BackgroundTriggerFilter
msbuild ..\..\..\build\BCI2000.sln /t:%SECTION%\%TARGET% /p:Configuration=Release /p:Platform=%VSCMD_ARG_TGT_ARCH%
if not "%1"=="" copy ..\..\..\%LOCATION%\%TARGET%*.exe "%1"\%LOCATION%\
set   TARGET=TrapFilter
msbuild ..\..\..\build\BCI2000.sln /t:%SECTION%\%TARGET% /p:Configuration=Release /p:Platform=%VSCMD_ARG_TGT_ARCH%
if not "%1"=="" copy ..\..\..\%LOCATION%\%TARGET%*.exe "%1"\%LOCATION%\
set   TARGET=RangeIntegrator
msbuild ..\..\..\build\BCI2000.sln /t:%SECTION%\%TARGET% /p:Configuration=Release /p:Platform=%VSCMD_ARG_TGT_ARCH%
if not "%1"=="" copy ..\..\..\%LOCATION%\%TARGET%*.exe "%1"\%LOCATION%\

cd /D "%OLDD%"
