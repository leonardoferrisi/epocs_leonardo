////////////////////////////////////////////////////////////////////////////////
// Authors:
// Description: DigitimerFilter implementation
// Filter uses the NI board to set an output 
////////////////////////////////////////////////////////////////////////////////

#include <stdlib.h>
#include <iostream>

#include "DigitimerFilter.h"
#include "BCIStream.h"

using namespace std;

RegisterFilter( DigitimerFilter, 2.D );

DigitimerFilter::DigitimerFilter()
: mD188Controller( NULL ), mDS8Controller( NULL )
{
  
}

DigitimerFilter::~DigitimerFilter()
{
  //if( mDS8Controller.ErrorCode == 0 ) mDS8Controller.CloseDS8();
}

void
DigitimerFilter::Publish()
{
  BEGIN_PARAMETER_DEFINITIONS

    "Stimulation:Digitimer int   AnalogOutput=            0     0     0  1 // deprecated - must be zero (boolean)", // TODO: remove
    "Stimulation:Digitimer int   EnableDS5ControlFilter=  0     0     0  1 // deprecated - must be zero (boolean)", // TODO: remove
    "Stimulation:Digitimer int   EnableDS8ControlFilter=  0     0     0  1 // enable DS8 control? (boolean)",       // TODO: could rename
    "Stimulation:Digitimer int   EnableD188ControlFilter= 0     0     0  1 // enable D188 control? (boolean)",      // TODO: could remove (set D188Channel=0 to disable)
    "Stimulation:Digitimer int   D188Channel=             1     1     1  8 // Initial channel to use if D188 Enabled",
    "Stimulation:Digitimer int   Biphasic=                1     1     1  2 // Options are 1=Yes 2=No (enumeration)",
    "Stimulation:Digitimer int   StimulationType=         1     1     1  6 // Options are 1=square 2=sine 3=linear_rising 4=linear_down 5=exp_rising 6=exp_down (enumeration)",
    "Stimulation:Digitimer float InitialCurrent=          0     1     0  % // Initial Current to Use for Stimulation \"mA\"",
    "Stimulation:Digitimer float PulseWidth=              0.5ms 0.5ms 0  % // PulseWidth of the Analog Stimuli",
    "Stimulation:Digitimer float InterphasicDelay=        0.1ms 0.1ms 0  % // Interphasic Delay of the Analog Stimuli",
  
  END_PARAMETER_DEFINITIONS
  
  BEGIN_STATE_DEFINITIONS
    "NeedsUpdating      1 0 0 0",  // TODO: change name to be more informative
    "CurrentAmplitude  16 0 0 0",  // Value represented as uA so we have 0-65535uA (or 65.535mA max)
  END_STATE_DEFINITIONS
}


void
DigitimerFilter::Preflight( const SignalProperties& inputProperties, SignalProperties& outputProperties ) const
{
  outputProperties = inputProperties; // this simply passes information through about SampleBlock dimensions, etc...
  
  if( Parameter( "AnalogOutput" ) ) bcierr << "AnalogOutput no longer supported - the parameter value must be 0." << endl;
  if( Parameter( "EnableDS5ControlFilter" ) ) bcierr << "The Digitimer DS5 is no longer supported. EnableDS5Controller must be 0." << endl;
	
  if( Parameter( "EnableDS8ControlFilter" ) )
  {
    DS8library::DS8Functions ds8Controller;
    if( ds8Controller.ErrorCode ) bcierr << "Unable to initialize Digitimer DS8 connection. Make sure the DS8 is on, and the DS8 software has been installed." << endl;
	  
    int    updateState      = State( "NeedsUpdating" );
    int    currentAmplitude = State( "CurrentAmplitude" );
    float  initialCurrent   = Parameter( "InitialCurrent" );
    int    stimType         = Parameter( "StimulationType" ); //***
    double pulseWidth       = Parameter( "PulseWidth" ).InMilliseconds() * 1000; //***
    if( ( pulseWidth < 50 ) && ( pulseWidth > 2000 ) ) //***
      bciwarn << "Warning: Pulse Width is " << pulseWidth << " microseconds, which is outside the 50-1000 range used for H-reflex conditioning" << std::endl;
  }
  
  if( Parameter( "EnableD188ControlFilter" ) )
  {  
    D188library::D188Functions d188Controller;
    if( d188Controller.ErrorCode ) bcierr << "Unable to initialize Digitimer D188 connection. Make sure the D188 is on, and the D188 software has been installed." << endl;
    int d188Channel = Parameter( "D188Channel" );
    //if( d188Channel < 1 || d188Channel > 8 ) bcierr << "Initial channel set to " << d188Channel << ". Must be an integer from 1 to 8" << endl;
  }
  
}


void
DigitimerFilter::Initialize( const SignalProperties& inputProperties, const SignalProperties& outputProperties )
{
  if( Parameter( "EnableDS8ControlFilter" ) )
  {
    mDS8Controller = new DS8library::DS8Functions;
    if( !mDS8Controller || mDS8Controller->ErrorCode ) throw bcierr << "failed to initialize DS8 library";
    
    int   stimulationType = Parameter( "StimulationType" );
    float current = Parameter( "InitialCurrent" ) * 10;
    float pulseWidth = Parameter( "PulseWidth" ).InMilliseconds() * 1000; 

    mDS8Controller->SetVariables(
      stimulationType,  // mode (default 2 = bipolar)
      NULL,             // polarity (default 2 = negative)
      current,          // current (default 0)
      pulseWidth,       // width (default 500 microseconds)
      NULL,             // recovery (default 100%)
      NULL,             // dwell (default 1 microsecond) 
      true              // update
    );
  }
  if( Parameter( "EnableD188ControlFilter" ) )
  {
    mD188Controller = new D188library::D188Functions;
    if( !mD188Controller || mD188Controller->ErrorCode ) throw bcierr << "failed to initialize D188 library";
    mD188Controller->SetChannel( Parameter( "D188Channel" ) );
  }
    
}

void
DigitimerFilter::Halt()
{
  delete mDS8Controller;  mDS8Controller  = NULL;
  delete mD188Controller; mD188Controller = NULL;
}

void
DigitimerFilter::StartRun()
{
  if( mDS8Controller && mDS8Controller->ErrorCode == 0 )
  {
    mDS8Controller->ToggleOutput( TRUE );
  }
}

void
DigitimerFilter::StopRun()
{
  if( mDS8Controller && mDS8Controller->ErrorCode == 0 )
  {
    mDS8Controller->ToggleOutput( FALSE );
  }
}

void
DigitimerFilter::Process( const GenericSignal& inputSignal, GenericSignal& outputSignal )
{
  outputSignal = inputSignal; // Pass the signal through unmodified.
  
  if( mDS8Controller && mDS8Controller->ErrorCode == 0 && State( "NeedsUpdating" ) )
  {
    int val = State( "CurrentAmplitude" ) / 100;
    //bciwarn << "Current:" << val << endl;
    mDS8Controller->SetVariables( NULL, NULL, val, NULL, NULL, NULL, TRUE );
    State( "NeedsUpdating" ) = 0;
  }
}
