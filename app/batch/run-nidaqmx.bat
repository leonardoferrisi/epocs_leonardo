#!../prog/BCI2000Shell
@cls & ..\prog\BCI2000Shell %0 %* #! && exit /b 0 || exit /b 1

system taskkill /F /FI "IMAGENAME eq NIDAQ_mx_Source.exe"
system taskkill /F /FI "IMAGENAME eq BiocircuitPNISource.exe"
system taskkill /F /FI "IMAGENAME eq gUSBampSource.exe"
system taskkill /F /FI "IMAGENAME eq FilePlayback.exe"
system taskkill /F /FI "IMAGENAME eq aReflexConditioningSignalProcessing.exe"
system taskkill /F /FI "IMAGENAME eq DummyApplication.exe"

system taskkill /F /FI "IMAGENAME eq NIDAQ_~1.exe"
system taskkill /F /FI "IMAGENAME eq BIOCIR~1.exe"
system taskkill /F /FI "IMAGENAME eq GUSBAM~1.exe"
system taskkill /F /FI "IMAGENAME eq FILEPL~1.exe"
system taskkill /F /FI "IMAGENAME eq AREFLE~1.exe"
system taskkill /F /FI "IMAGENAME eq REFLEX~1.exe"
system taskkill /F /FI "IMAGENAME eq DUMMYA~1.exe"

change directory $BCI2000LAUNCHDIR

set environment MODE   $1   # "standalone" or "skinned"
set environment SOURCE $2   # "nidaqmx" or "gusbamp" or "pni" or a filename for replay 
set environment CUSTOM $3   # empty, or a path to a BCI2000 script file

if [ $MODE   == "" ]; set environment MODE   "standalone"; end
if [ $SOURCE == "" ]; set environment SOURCE "nidaqmx";    end

if [ $MODE == "standalone" ]; show window; end
set title ${extract file base $0}
reset system

if [ $EPOCSTIMESTAMP == "" ]; set environment EPOCSTIMESTAMP $YYYYMMDD-$HHMMSS; end
startup system localhost --SystemLogFile=../../system-logs/$EPOCSTIMESTAMP-operator.txt

if [ ${is file $SOURCE} ]
	start executable FilePlayback     --local --EvaluateTiming=0 --PlaybackFileName=$SOURCE
	set environment SOURCE "replay"
elseif [ $SOURCE == "nidaqmx" ]
	start executable NIDAQ_mx_Source  --local
elseif [ $SOURCE == "gusbamp" ]
	start executable gUSBampSource    --local
elseif [ $SOURCE == "pni" ]
	start executable BiocircuitPNISource --local
else
	error unable to get data from source "$SOURCE"
end
start executable DummyApplication                   --local
if [ $SOURCE != "replay" ]; sleep 1; end
start executable aReflexConditioningSignalProcessing --local --NumberOfThreads=1

add parameter Storage:Session                        string   SessionStamp=           %     %  % % // 
add parameter Application:Operant%20Conditioning     string   ApplicationMode=       ST     %  % % // 
add parameter Application:Operant%20Conditioning     float    BackgroundScaleLimit=  20mV 20mV 0 % // 
add parameter Application:Operant%20Conditioning     float    ResponseScaleLimit=    20mV 20mV 0 % // 
add parameter Application:Operant%20Conditioning     float    BaselineResponseLevel=  %    8mV 0 % //
add parameter Application:EPOCS                      float    MwaveTarget=           30mV 30mV % % //
add parameter Application:EPOCS                      float    MwavePercentage=       20   20   % % //
add parameter Application:EPOCS                      float    TargetPercentile=      66   66   % % //

wait for connected

load parameterfile ../parms/base-nidaqmx.prm  # nidaqmx is the base from which all the others diverge

if [ $SOURCE == "replay" ]
	# do nothing - just use the parameters from the file (vital for SamplingRate and SourceCh*, and desirable for SampleBlockSize - but note that epocs.py may overrule many parameters)
elseif [ $SOURCE == "nidaqmx" ]
	load parameterfile ../parms/NIDigitalOutputPort.prm
	load parameterfile ../parms/base-nidaqmx.prm	# TODO: ideally we should delete this line, or swap its order relative to the line above. Right now it overwrites the site-specific NIDigitalOutputPort.prm (which has presumably-more-up-to-date SourceChGain values) and so makes NIDigitalOutputPort.prm useless. However, the implementation has always done it this way (bugged?) so we're being cautious about making the change where MUSC's setups are concerned.
elseif [ $SOURCE == "gusbamp" ]
	load parameterfile ../parms/base-gUSBamp.prm # TODO: need to hack the gUSBampADC to allow DigitalOutputEx to be (optionally) evaluated early (before sample-block acquisition rather than after)
elseif [ $SOURCE == "pni" ]
	load parameterfile ../parms/base-BiocircuitPNI.prm
end

if [ $MODE == "standalone" ]
	set parameter VisualizeTiming 1
	set parameter VisualizeSource 1
	set parameter VisualizeSpatialFilter 1
	set parameter VisualizeIIRBandpass 1
	set parameter VisualizeBackgroundAverages 1
	set parameter VisualizeTrapFilter 1
	set parameter VisualizeRangeIntegrator 1
else
	set parameter OutputMode 0
end

if [ $CUSTOM ]
	execute script $CUSTOM
end

if [ $MODE == "standalone" ]
	setconfig
	set state Running 1
end
