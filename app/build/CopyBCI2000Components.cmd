#!/usr/bin/env python
""" " 2>NUL
@echo off
cls
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
setlocal
set "CUSTOMPYTHONHOME=%PYTHONHOME_EPOCS%
if "%CUSTOMPYTHONHOME%"=="" goto :skipconfig
set "PYTHONHOME=%CUSTOMPYTHONHOME%
set "PATH=%PYTHONHOME%;%PATH%
:skipconfig

python "%~f0" %*
endlocal
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
goto :eof
" """

__doc__ = """
{execName} [-n] [BCI2000ROOT] [RESTRICT_TO_FILENAMES...]

For any file that's git-tracked inside `app/prog` here, see if there
is a corresponding file in the `prog` directory of `BCI2000ROOT`. If
there is, copy it here.

-n:    Dry run: print what would be done, but don't do it.
-s:    Run `SignBinaries.cmd` on any copied `.dll` and `.exe` files

The default `BCI2000ROOT` is `../../../bci2000-svn/HEAD` relative
to the location of this script.

Optional further arguments (`RESTRICT_TO_FILENAMES`) are the
basenames of files that you want to include in the copy operation.
Filenames can contain `?` and `*` wildcards. Arguments that begin
with `!` are considered exclusion rather than inclusion patterns.
If you do not specify any inclusion patterns, all possible files are
considered.

Example::

	{execName} \neurotech\bci2000-svn\HEAD  !BCI2000Remote* -s -n

"""
import os, sys, shutil, shlex, subprocess, fnmatch

if sys.version < '3': bytes = str
else: unicode = str; basestring = ( unicode, bytes )
def IfStringThenRawString( x ):
	return x.encode( 'utf-8' ) if isinstance( x, unicode ) else x
def IfStringThenNormalString( x ):
	if str is bytes or not isinstance( x, bytes ): return x
	try: return x.decode( 'utf-8' )
	except: pass
	try: return x.decode( sys.getfilesystemencoding() )
	except: pass
	return x.decode( 'latin1' ) # bytes \x00 to \xff map to characters \x00 to \xff (so, in theory, cannot fail)

def Bang( cmd, shell=False, stdin=None, cwd=None, raiseException=False ):
	windows = sys.platform.lower().startswith('win')
	# If shell is False, we have to split cmd into a list---otherwise the entirety of the string
	# will be assumed to be the name of the binary. By contrast, if shell is True, we HAVE to pass it
	# as all one string---in a massive violation of the principle of least surprise, subsequent list
	# items would be passed as flags to the shell executable, not to the targeted executable.
	# Note: Windows seems to need shell=True otherwise it doesn't find even basic things like ``dir``
	# On other platforms it might be best to pass shell=False due to security issues, but note that
	# you lose things like ~ and * expansion
	if isinstance( cmd, str ) and not shell:
		if windows: cmd = cmd.replace( '\\', '\\\\' ) # otherwise shlex.split will decode/eat backslashes that might be important as file separators
		cmd = shlex.split( cmd ) # shlex.split copes with quoted substrings that may contain whitespace
	elif isinstance( cmd, ( tuple, list ) ) and shell:
		quote = '"' if windows else "'"
		cmd = ' '.join( ( quote + item + quote if ' ' in item else item ) for item in cmd )
	try: sp = subprocess.Popen( cmd, shell=shell, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE )
	except OSError as exc: returnCode, output, error = 'command failed to launch', '', str( exc )
	else: output, error = [ IfStringThenNormalString( x ).strip() for x in sp.communicate( stdin ) ]; returnCode = sp.returncode
	if raiseException and returnCode:
		if isinstance( returnCode, int ): returnCode = 'command failed with return code %s' % returnCode
		raise OSError( '%s:\n    %s\n    %s' % ( returnCode, cmd, error ) )
	return returnCode, output, error


if __name__ == '__main__':
	root = os.path.realpath( os.path.join( __file__, '..', '..' ) )
	
	args = sys.argv[ 1: ]
	
	dry = False
	while '-n' in args: args.remove( '-n' ); dry = True
		
	sign = False
	while '-s' in args: args.remove( '-s' ); sign = True
		
	help = False
	while '-h' in args: args.remove( '-h' ); help = True
	if help: raise SystemExit( __doc__.format( execName=os.path.basename( sys.argv[ 0 ] ) ) )
		
	bci2000 = args.pop( 0 ) if args else ''
	if not bci2000: bci2000 = os.path.join( root, '..', '..', 'bci2000-svn', 'HEAD' )
	bci2000 = os.path.realpath( os.path.expanduser( bci2000 ) )
	if os.path.split( bci2000 )[ -1 ] == 'prog': bci2000 = os.path.dirname( bci2000 )
	
	
	NOT = '!'
	restrictToFileNames = [ arg                for arg in args if not arg.startswith( NOT ) ]
	excludeFileNames    = [ arg[ len( NOT ): ] for arg in args if     arg.startswith( NOT ) ]
	
	sign = [ os.path.join( root, 'build', 'SignBinaries.cmd' ) ] if sign else []
	for partial in [ [ 'prog' ], [ 'tools', 'cmdline' ] ]:
		srcDir = os.path.join( bci2000, *partial )
		dstDir = os.path.realpath( os.path.join( root, *partial ) )
		os.chdir( dstDir )
		returnCode, out, err = Bang( [ 'git', 'ls-files' ], raiseException=True )
		files = out.split( '\n' )
		verb = 'would copy' if dry else 'copying'
		for file in files:
			if restrictToFileNames and not any( fnmatch.fnmatch( file, pattern ) for pattern in restrictToFileNames ): continue
			if excludeFileNames    and     any( fnmatch.fnmatch( file, pattern ) for pattern in excludeFileNames ):
				print( 'skipping due to exclusion pattern: {}\n'.format( file ) )
				continue
			srcFile = os.path.join( srcDir, file )
			dstFile = os.path.join( dstDir, file )
			if os.path.isfile( srcFile ):
				print( '{} {}\n{} to {}\n'.format( verb, srcFile, ' ' * len( verb ), dstFile ) )
				#returnCode, out, error = Bang( [ 'file', srcFile ] ); print( error if returnCode else out ) # `file` will only be available when invoked from git bash or cygwin.  For some files, this shows whether it's a 32-bit or 64-bit binary. For others (bundled modules) it unfortunately just says the file is a zip archive of some kind.
				if not dry:
					shutil.copy2( srcFile, dstFile )
				if sign and dstFile.lower().endswith( ( '.exe', '.dll' ) ):
					sign.append( dstFile )
	if len( sign ) > 1:
		os.chdir( os.path.join( root, 'build' ) )
		sign = [ os.path.basename( sign[ 0 ] ) ] + [ os.path.relpath( x, os.getcwd() ) for x in sign[ 1: ] ] # because signtool.exe seems to have trouble/weird behaviour with path resolution
		
		verb = 'would then sign' if dry else 'signing'
		print( '{} with:\n\n    cd /D "{}"\n    {}\n\n'.format( verb, os.getcwd(), ' '.join( '"' + x + '"' for x in sign ) ) )
		if not dry:
			returnCode, out, error = Bang( sign )
			print( error if returnCode or error else out )
