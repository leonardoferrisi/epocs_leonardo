#!/usr/bin/env python
""" " 2>NUL
@echo off
cls
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
setlocal
set "CUSTOMPYTHONHOME=%PYTHONHOME_EPOCS%
if "%CUSTOMPYTHONHOME%"=="" goto :skipconfig
set "PYTHONHOME=%CUSTOMPYTHONHOME%
set "PATH=%PYTHONHOME%;%PATH%
:skipconfig

python "%~f0" %*
endlocal
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::
goto :eof
" """

__doc__ = """
{execName} -h
{execName} [-n] [-f] [BCI2000ROOT]

Make a junction, under `BCI2000ROOT` at `src/custom`,  pointing
at the `src/custom` directory here. Windows only.

-h:   Print this usage message and exit.
-n:   Dry run: print what would be done but don't actually do it.
-f:   Force: remove an existing `src/custom` if it is a junction
      (not if it is an ordinary directory or file).

The default `BCI2000ROOT` is `../../../bci2000-svn/HEAD` relative
to the location of this script.
"""



import os, re, sys, shlex, shutil, subprocess

if sys.version < '3': bytes = str
else: unicode = str; basestring = ( unicode, bytes )
def IfStringThenRawString( x ):
	return x.encode( 'utf-8' ) if isinstance( x, unicode ) else x
def IfStringThenNormalString( x ):
	if str is bytes or not isinstance( x, bytes ): return x
	try: return x.decode( 'utf-8' )
	except: pass
	try: return x.decode( sys.getfilesystemencoding() )
	except: pass
	return x.decode( 'latin1' ) # bytes \x00 to \xff map to characters \x00 to \xff (so, in theory, cannot fail)

def Bang( cmd, shell=False, stdin=None, cwd=None, raiseException=False ):
	windows = sys.platform.lower().startswith('win')
	# If shell is False, we have to split cmd into a list---otherwise the entirety of the string
	# will be assumed to be the name of the binary. By contrast, if shell is True, we HAVE to pass it
	# as all one string---in a massive violation of the principle of least surprise, subsequent list
	# items would be passed as flags to the shell executable, not to the targeted executable.
	# Note: Windows seems to need shell=True otherwise it doesn't find even basic things like ``dir``
	# On other platforms it might be best to pass shell=False due to security issues, but note that
	# you lose things like ~ and * expansion
	if isinstance( cmd, str ) and not shell:
		if windows: cmd = cmd.replace( '\\', '\\\\' ) # otherwise shlex.split will decode/eat backslashes that might be important as file separators
		cmd = shlex.split( cmd ) # shlex.split copes with quoted substrings that may contain whitespace
	elif isinstance( cmd, ( tuple, list ) ) and shell:
		quote = '"' if windows else "'"
		cmd = ' '.join( ( quote + item + quote if ' ' in item else item ) for item in cmd )
	try: sp = subprocess.Popen( cmd, shell=shell, cwd=cwd, stdout=subprocess.PIPE, stderr=subprocess.PIPE )
	except OSError as err: return err, '', ''
	output, error = [ IfStringThenNormalString( x ).strip() for x in sp.communicate( stdin ) ]
	if raiseException and sp.returncode:
		if raiseException == 1: raiseException = OSError
		raise raiseException( 'command failed with return code %s:\n%s\n%s' % ( sp.returncode, cmd, error ) )
	return sp.returncode, output, error

def MakeJunction( junction, target, dry=False, force=False ):
	parentDir, name = os.path.split( junction )
	if os.path.exists( junction ):
		allGood = 'Junction already exists: "%s"\nIt already points here, to "%s"' % ( junction, target )
		if hasattr( os.path, 'samefile' ) and os.path.samefile( junction, target ):
			raise OSError( allGood )
		msg ='error: There is an existing item called "%s"' % junction
		returnCode, out, err = Bang( [ 'dir', parentDir ], shell=True, raiseException=True ) # NB: fails without shell=True (mklink not found)
		entries = out.split( '\n' )
		for entry in entries:
			m = re.match( r'.*?(\<(?P<type>.+)\>)?\s+custom(\s*\[(?P<target>.+)\])?\s*$', entry )
			if 'custom' in entry and not m: zark
			if m:
				type = m.group( 'type' )
				existingTarget = m.group( 'target' )
				if type and type.lower() == 'dir': msg += '\n       It is an ordinary directory.';
				elif type and type.lower() == 'junction':
					if existingTarget == target: msg = allGood
					else: msg += '\n       It is a junction, but has a different target: "%s"\n       Use the -f flag to force removal and renewal of this junction.' % existingTarget
					if force:
						print( re.sub( r'(^|\n)(error:)?\s+', r'\1', msg, re.S ) )
						print( '%s existing junction with rmdir' % ( 'Would remove' if dry else 'Removing' ) )
						if not dry:
							Bang( [ 'rmdir', junction ], shell=True, raiseException=True )
							print( 'Removed\n' )
						msg = ''
				else: msg += '\n       It does not appear to be a directory or a junction.'
				break
		if( msg ): raise OSError( msg )
		
	cmd = [ 'mklink', '/J', name, target ]
	cmdString = ' '.join( ( '"%s"' % piece ) if '\\' in piece else piece for piece in cmd )
	if dry:
		print( 'Would run::\n    cd "%s"\n    %s\n' % ( parentDir, cmdString ) )
	else:
		print( 'cd "%s"' % parentDir )
		previousDirectory = os.getcwd()
		try:
			os.chdir( parentDir )
			print( cmdString )
			returnCode, out, err = Bang( cmd, shell=True, raiseException=True ) # NB: fails without shell=True (mklink not found)
			print( out )
			#os.system( cmdString ) # dirty fallback works either way
		finally:
			os.chdir( previousDirectory )

if __name__ == '__main__':
	
	args = sys.argv[ 1: ]
	
	dry = False
	while '-n' in args: args.remove( '-n' ); dry = True
		
	force = False
	while '-f' in args: args.remove( '-f' ); force = True
		
	help = False
	while '-h' in args: args.remove( '-h' ); help = True
	if help: raise SystemExit( __doc__.format( execName=os.path.basename( sys.argv[ 0 ] ) ) )
		
	bci2000 = args.pop( 0 ) if args else ''
	if not bci2000: bci2000 = os.path.join( __file__, '..', '..', '..', '..', 'bci2000-svn', 'HEAD' )
	bci2000 = os.path.realpath( os.path.expanduser( bci2000 ) )
	junction = bci2000
	if os.path.split( junction )[ -1 ] != 'custom':
		if os.path.split( junction )[ -1 ] != 'src':
			junction = os.path.join( junction, 'src' )
		junction = os.path.join( junction, 'custom' )

	target = os.path.realpath( os.path.join( __file__, '..', '..', 'src', 'custom' ) )
	
	try: MakeJunction( junction, target, dry=dry, force=force )
	except OSError as err: raise SystemExit( str( err ) )

