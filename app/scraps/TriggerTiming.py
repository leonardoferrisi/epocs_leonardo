import os
import sys
import glob

class UsageError( Exception ): pass

def ProcessLog( samplesPerSecond=None, logFilePattern=None, index=-1 ):
	try: samplesPerSecond = float( samplesPerSecond )
	except: raise UsageError( "The first argument should be sampling rate in Hz." )
	if not logFilePattern: raise UsageError( "The second argument should be one of the following:\n- a log file name,\n- a glob pattern that matches log files, or\n- a directory containing log files named *-operator.txt" )
	try: index = int( index )
	except: raise UsageError( "The third argument (if supplied) should be a numeric index into the set\nof filename matches (possibly negative, to count backwards from the end).\nThe default is -1, which gets the most recent file (assuming their names\nstart with an ISO datestamp)." )
	filePattern = os.path.expanduser( logFilePattern )
	if os.path.isdir( filePattern ): filePattern = os.path.join( filePattern, '*-operator.txt' )
	files = sorted( glob.glob( filePattern ) )
	fileName = os.path.realpath( files[ index ] )
	print( fileName )
	with open( fileName ) as fileHandle:
		elapsed = []
		start = None
		for line in fileHandle:
			line = line.strip().strip( '.' ).lower()
			if 'trigger enabled after evaluating sample ' in line:
				start = int( line.strip().split()[ -1 ] )
			if 'trigger detected on sample ' in line:
				end = int( line.strip().split()[ -1 ] )
				elapsed.append( ( end - start ) / samplesPerSecond * 1000.0 )
				start = None
	n = float( len( elapsed ) )
	mean = sum( elapsed ) / n if n else float( 'nan' )
	std = ( sum( ( x - mean ) ** 2.0 for x in elapsed ) / ( n - 1.0 ) ) ** 0.5
	result = dict( file=fileName, elapsed=elapsed, mean=mean, std=std, n=n, min=min( elapsed ) if n else float( 'nan' ), max=max( elapsed ) if n else float( 'nan' ) )
	print( '\nelapsed = %s' % elapsed )
	if n: print( '\n# {mean:.3f} ms +/- {std:.3f} SD (N={n:g}, min={min:.3f}, max={max:.3f})'.format( **result ) )
	else: print( '\n# no triggers found' )
	return result
	
if __name__ == '__main__':
	try: result = ProcessLog( *sys.argv[ 1: ] )
	except UsageError as err: raise SystemExit( '\n%s' % err )
	
