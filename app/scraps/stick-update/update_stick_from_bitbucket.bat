#!/usr/bin/env bash # 2>NUL
:<<@GOTO:EOF
: ###########################
: ### Windows ###############

@set "BARE_REPO_NAME=epocs.git"
@set "BARE_REPO_PATH=%~dp0%BARE_REPO_NAME%"

git -C "%BARE_REPO_PATH%" fetch -v origin "*:*" && ^
@echo latest: && ^
@git -C "%BARE_REPO_PATH%" log --graph --date-order --date=iso-local --abbrev-commit --decorate=short --all --max-count=1

@pause
: ###########################
@GOTO:EOF
: ###########################
: ### POSIX  ################

set -e

LOCATE_CODE="import os,sys;print(os.path.dirname(os.path.realpath(sys.argv[1])))"  # darwin doesn't have the realpath utility
HERE=$( python -c "$LOCATE_CODE" "$0" )
BARE_REPO_NAME="epocs.git"
BARE_REPO_PATH="$HERE/$BARE_REPO_NAME"

git -C "$BARE_REPO_PATH" fetch -v origin "*:*"
git -C "$BARE_REPO_PATH" log --graph --date-order --date=iso-local --abbrev-commit --decorate=short --all

: ###########################
