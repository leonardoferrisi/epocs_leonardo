# this is useful for debugging in standalone mode (removes contingency of stim on background EMG)
# 
set parameter Background    matrix     BackgroundChannels=         2 { Input%20Channel Subtract%20Mean? Norm Min%20Amplitude Max%20Amplitude Feedback%20Weight }                                                               EMG1              yes         1        %           %               1.0             EMG2              yes         1        %           %               0.0          // specification of signal channels used to monitor baseline compliance
