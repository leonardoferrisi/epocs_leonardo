# this is the place to experiment with gradient transformations and corrections for grid slippage
# Michael's first zero-sum gradient transformation (order 4,4,2,  center 0,0, angle 0)
set parameter Filtering matrix   SpatialFilter=    33 { In Out Wt } acq01 EMG1 -6.1661 acq02 EMG1 -1.3819 acq03 EMG1 5.5809 acq04 EMG1 9.4184 acq05 EMG1 1.2970 acq06 EMG1 -2.8804 acq07 EMG1 5.9757 acq08 EMG1 11.1802 acq09 EMG1 9.2093 acq10 EMG1 -3.4608 acq11 EMG1 -1.0059 acq12 EMG1 2.4298 acq13 EMG1 4.6483 acq14 EMG1 4.4749 acq15 EMG1 0.7353 acq16 EMG1 1.0059 acq17 EMG1 -2.4298 acq18 EMG1 -4.6483 acq19 EMG1 -4.4749 acq20 EMG1 -0.7353 acq21 EMG1 2.8804 acq22 EMG1 -5.9757 acq23 EMG1 -11.1802 acq24 EMG1 -9.2093 acq25 EMG1 3.4608 acq26 EMG1 6.1661 acq27 EMG1 1.3819 acq28 EMG1 -5.5809 acq29 EMG1 -9.4184 acq30 EMG1 -1.2970 acq31 EMG2 33.3333 acq32 EMG2 -33.3333 TRIG TRIG 1.0


# uncomment the line below if the 30-channel octagon grid is not connected (only the two contacts that contribute to the bipolar channel will contribute to bias estimation)
#set parameter Source    intlist  ExcludeFromBias=  30  1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30   // one-based indices (according to device's numbering) of channels to exclude from computation of bias for common-mode cancellation

set parameter VisualizeTiming 1
set parameter VisualizeSource 1
set parameter VisualizeSpatialFilter 1
set parameter VisualizeIIRBandpass 1