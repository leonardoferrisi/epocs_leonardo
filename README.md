EPOCS stands for Evoked Potential Operant Conditioning System.  It is an experimental system for
improving muscle control in people who have neuromuscular disorders - for example, to improve gait
in people who have difficulty walking following an incomplete spinal-cord injury. It works by
allowing a researcher or therapist to train a patient to increase or decrease the size of their
reflex responses (for example, the knee-jerk reflex). The scientific principles behind this are
explained in research papers published by Dr. Aiko Thompson, Dr. Jonathan Wolpaw and colleagues.
[Their 2013 paper in the Journal of Neuroscience](http://jneurosci.org/content/33/6/2365)
demonstrates how the approach can improve walking in people with spinal-cord injuries.

EPOCS was developed in 2013-2014 by [Dr. Jeremy Hill](http://schalklab.org/jhill), in Dr. Thompson's
Translational Neurological Research Laboratory at [Helen Hayes Hospital](http://www.helenhayeshospital.org/).
EPOCS consists of custom-written software as well as carefully chosen and configured hardware.

The software is based on [BCI2000](http://bci2000.org), a widely-used free software platform that
allows biological signals to be measured and processed in real time for applications in research
and in translational clinical neurotechnology. There is no dependency on proprietary third-party
software beyond that required to interface with the data-acquisition hardware (in other words. no
Matlab, LabView or similar licenses are required).

The hardware consists mainly of amplifiers that measure electrical signals from muscles, a data
acquisition board, and a stimulator that delivers a brief electrical pulse to elicit a reflex. The
amplifier and stimulator both use electrodes that are stuck temporarily to the skin surface.  The
system can also be configured to use a mechanical stimulator (for conditioning natural stretch
reflexes) or a transcanial magnetic stimulator (for investigating the role of responses generated
by the brain rather than reflexes from the spinal cord).