`app/parms/NIDigitalOutputPort.prm`  need not always be loaded (not for
non-NI sources), yet its absence causes a crash in `run-nidaqmx.bat`.
Most of `NIDigitalOutputPort.prm`  (e.g. the calibrated gains) is lost
because it is overwritten by `app/parms/base-nidaqmx.prm` (loaded after).
This means Aiko has been using non-calibrated gains all this time, and
any "fix" for this should be wary of the sudden transition in gain values.
The two exceptions are:  

- `BoardNumber= 1` which does not appear at all in `base-nidaqmx.prm`, and

- `FilterExpressions= ...` which appears with a typo (missing final `s`)
  in `base-nidaqmx.prm` and therefore silently fails to overwrite the
  value we got from `NIDigitalOutputPort.prm`.
  
  
`aReflexConditioningSignalProcessing.exe` has the following problems:
- It won't accept an empty `FilterExpressions` parameter
- Possibly because of this, it requires nicaiu.dll to be present (i.e. NI DAQmx installed)
- It requires an MSVC redistributable runtime.
- It needs the SpatialFilter
- It doesn't control the DS8R well

All but the last point above are/shoud be fixed in the pni branch.

- verify that DigitimerFilter.cpp works. I had previously noted that
  it "doesn't control the DS8R well." I wish I had noted more detail
  about that. However, it would now be relatively easy to replace
  Amir's `DS8library.cpp` with an adaptation of my `ds8.cpp` from the
  `ds8-try` directory.
- Fix analog output generation in the new `NIDAQmxADC`
- Put the new `NIDAQmxADC` into BCI2000 itself.
- Recreate / fix `NISetup` so that it generates the right parameter (now
  named `OutputSignals` and formatted slightly differently). And fix it
  so that the resulting file gets loaded in the correct sequence!
- Figure out whether we still need to generate analog signals, and if
  so: write a filter that writes the appropriate signals into a State
  channel (so that `NIDAQmxADC`, with its hopefully-fixed sample-by-sample
  analog generator, can then read that).
- Can gUSBampADC be hacked so that its own DigitalOutputEx timing is good?
- `aReflexConditioningSignalProcessing` needs eventually to be removed and
  renamed back to just `ReflexConditioningSignalProcessing`.

Persistent defaults for new subjects, like  { '_DigitimerEnable' : 'on', '_STBackgroundEnable' : 'yes', } can be added to epocs.ini (or other --defaults file as of e821a54)

63   VisualizeSource without custom script (settings checkbox)  - can also be set in epocs.ini (or other --defaults file as of e821a54)
47   Stimulus pulse width      done in e821a54
64   Minimum stim interval     done in e821a54
62   gUSBampSource module crash (shipped version) or parameter incompatibility (newer version)


EPOCS-Offline:
	While starting up, and while loading a large file: provide feedback that the application isn't dead
	Export trial number in CSV
	quantify correlation with template

Resolve policy on default-replay.dat  SourceChGain
    possibly need new setting that reflects the analog amp gain

Make new ExampleData.pk
    Obtain a good soleus VC session
    Move --dumpdata to offline mode (it's faster!)
    split into separate files, e.g. ExampleDataRC.pk
        use variable keys with standard names inside the file: signal and samplingFrequencyHz
        resample on load if the saved sampling frequency doesn't match the current setting

Exception in Tkinter callback
Traceback (most recent call last):
  File "epocs.py", line 2256, in cancel
    self.Log(type=type)
  File "epocs.py", line 2744, in Log
    for button in self.MatchWidgets( type, 'button', 'log' ): button[ 'state' ] = 'disabled'
TclError: invalid command name ".242991824.253969992.253970112.253970192.253970672"
